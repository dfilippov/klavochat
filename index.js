const blessed = require('blessed');
const Chat = require('./lib/chat');

const screen = blessed.screen({
  smartCSR: true,
  ignoreLocked: ['tab'],
});

const chat = new Chat(screen);
screen.key(['escape', 'q'], () => chat.close());
screen.key(['C-c'], () => chat.client.disconnect());
screen.key(['tab'], () => screen.focusNext());
screen.render();
