const EventEmitter = require('events');
const blessed = require('blessed');
const config = require('../gui.json');
const formatPresence = require('./presence-format');

const obj = Symbol();
const list = Symbol();

module.exports = class ParticipantsList extends EventEmitter{
  constructor () {
    super();
    this[obj] = blessed.list(config.participants_list);
    this[obj].on('select', (widget) => {
      const found = this[list].find((item) => item.text === widget.getText());
      super.emit('select', found);
    });
    this[list] = [];
  }

  render () {
    this[obj].setItems(this.representation);
    this[obj].screen.render();
  }

  set (id, presence) {
    const formated = formatPresence(presence);
    let item = this.find(id);
    if (item) {
      item.text = formated;
      item.login = presence.user.login;
    } else {
      this[list].push({ id, login: presence.user.login, text: formated });
    }

    this.render();
  }

  find (id) {
    return this[list].find((item) => item.id === id);
  }

  remove (id) {
    const i = this[list].findIndex((item) => item.id === id);
    if (i < 0) {
      return false;
    }

    this[list].splice(i, 1)
    this.render();
  }

  get representation() {
    return this[list].sort((a, b) => {
      if (a.text < b.text) {
        return -1;
      }
      if (a.text > b.text) {
        return 1;
      }
      return 0;
    }).map((item) => item.text);
  }

  get widget () {
    return this[obj];
  }
}
