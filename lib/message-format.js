module.exports = (message) => {
  if (message.user) {
    let color = message.user.color;
    color = color === '#000000' ? '#333' : color;
    return `{${color}-fg}${message.user.login}:{/} ${message.body}`
  } else {
    return `{#f00-fg}${message.body}{/}`;
  }
};
