const parseUser = require('./user-parser');

module.exports = (ltxe) => {
  const delay = ltxe.getChild('delay');
  return {
    from: ltxe.attr('from'),
    to: ltxe.attr('to'),
    date: delay ? new Date(delay.attr('stamp')) : new Date(),
    body: ltxe.getChildText('body'),
    type: ltxe.attr('type'),
    user: parseUser(ltxe.getChild('x', 'klavogonki:userdata')),
  };
};
