module.exports = (presence) => {
  return `${presence.user.login} ${presence.user.moderator ? '*' : ''}`;
};
