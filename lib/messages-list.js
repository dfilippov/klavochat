const blessed = require('blessed');
const config = require('../gui.json');
const formatMessage = require('./message-format');

const obj = Symbol();
const rooms = Symbol();

module.exports = class MessagesList {
  constructor () {
    this[obj] = blessed.box(config.messages_list);
    this[rooms] = {};
    this[obj].on('scroll', () => {
      this.userScrollPos = this[obj].getScrollPerc();
    });
    this.userScrollPos = 100;
  }

  render () {
    if (this.userScrollPos === 100) {
      this[obj].setScrollPerc(100);
    }
    this[obj].screen.render();
  }

  add (message, room) {
    if (!this.activeRoom) {
      this.activeRoom = room;
    }

    this.create(room);
    const formated = formatMessage(message);
    this[rooms][room].push(formated);
    if (room === this.activeRoom) {
      this[obj].pushLine(formated);
      this.render();
    }
  }

  create (room) {
    if (!(room in this[rooms])) {
      this[rooms][room] = [];
    }
  }

  open (room) {
    if (!(room in this[rooms])) {
      return false;
    }

    this.activeRoom = room;
    this.userScrollPos = 100;
    this[obj].setContent(this[rooms][room].join('\n'));
    return this.render();
  }

  get widget () {
    return this[obj];
  }
}
