const blessed = require('blessed');
const config = require('../gui.json').tabs;

const obj = Symbol();
const hash = Symbol();
const active = Symbol();

module.exports = class Tabs {
  constructor () {
    this[obj] = blessed.listbar(config);
    this[hash] = {};
  }

  set (id, tab) {
    if (id in this[hash]) {
      return this.active !== id ? this.setTabState(id, tab.state) : false;
    }
    // blessed add() method takes only text and callback parameters :(
    this[obj].add(tab.text, () => {
      this.active = id;
      tab.callback();
    });
    this[hash][id] = Object.assign(tab, {
      widget: this[obj].items[this[obj].items.length - 1],
    });
    return !this.active ? this.active = id : this.setTabState(id, tab.state);
  }

  setTabState (id, state) {
    let tab = this[hash][id];
    if (!tab) {
      return false;
    }

    tab.state = state;
    let style = config.style.item;
    switch (state) {
      case 'unread': style = config.style.unread; break;
      case 'selected': style = config.style.selected; break;
    }

    for (let field in style) {
      tab.widget.style[field] = style[field];
    }
    this[obj].screen.render();
  }

  close (id) {
    let tab = this[hash][id];
    if (!tab) {
      return false;
    }

    this[obj].removeItem(tab.widget);
    if (id === this.active) {
      this[obj].selectTab(0);
    }
    delete this[hash][id];
    this[obj].screen.render();
  }

  get length () {
    return Object.keys(this[hash]).length;
  }

  get active () {
    return this[active] || null;
  }

  set active (id) {
    this.setTabState(this.active, 'item');
    this.setTabState(id, 'selected');
    this[active] = id;
  }

  get widget () {
    return this[obj];
  }
}
