const EventEmitter = require('events');
const xmpp = require('node-bosh-xmpp-client');
const config = require('../config.json');
const parsePresence = require('./presence-parser');
const parseMessage = require('./message-parser');
const presence = require('./presence');

const client = Symbol();

module.exports = class Client extends EventEmitter {
  constructor () {
    super();
    this[client] = new xmpp.Client(config.jid, config.password, config.bosh, config.route);
    this[client].on('error', (exception) => {
      throw new Error(exception);
      super.emit('error', exception)
    });
    this[client].on('offline', (reason) => super.emit('offline', reason));
    this[client].on('online', () => this.onConnect());
    this[client].on('stanza', (ltxe) => this.onStanza(ltxe));
  }

  onConnect () {
    super.emit('online');
    this[client].send(presence.common);
    this[client].send(presence.room);
  }

  sendMessage (message, room) {
    const type = room === config.room ? 'groupchat' : 'chat';
    const fakeMessage = {
      from: room,
      to: room,
      date: new Date(),
      body: message,
      type,
      user: {
        login: config.login,
        color: config.color,
        avatar: config.avatar,
      },
    };
    this[client].sendMessage(room, message, type);
    super.emit('message', fakeMessage);
  }

  onStanza (ltxe) {
    super.emit('stanza', ltxe);
    if (ltxe.name === 'message') {
      return super.emit('message', parseMessage(ltxe));
    }

    if (ltxe.name === 'presence') {
      return super.emit('presence', parsePresence(ltxe));
    }
  }

  disconnect () {
    this[client].disconnect();
  }
}
