const parseUser = require('./user-parser');

module.exports = (ltxe) => {
  const x = ltxe.getChild('x', 'http://jabber.org/protocol/muc#user');
  let presence = {
    type: ltxe.attr('type'),
    status: ltxe.getChildText('status'),
    from: ltxe.attr('from'),
    to: ltxe.attr('to'),
  }

  if (!x) {
    return presence;
  }

  const item = x.getChild('item');
  return Object.assign(presence, {
    jid: item.attr('jid'),
    affiliation: item.attr('affiliation'),
    role: item.attr('role'),
    user: parseUser(ltxe.getChild('x', 'klavogonki:userdata')),
  });
};
