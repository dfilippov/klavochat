module.exports = (ltxe) => {
  if (!ltxe) {
    return null;
  }

  const user = ltxe.getChild('user');
  if (!user) {
    return null;
  }

  return {
    login: user.getChildText('login'),
    avatar: user.getChildText('avatar'),
    color: user.getChildText('background'),
    moderator: user.getChildText('moderator') === '1',
    gameId: ltxe.getChildText('game_id'),
  };
};
