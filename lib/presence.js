const xmpp = require('node-bosh-xmpp-client');
const config = require('../config.json');

module.exports = {
  common: xmpp.$pres({ xmlns: 'jabber:client' }).c('priority').t(100),
  room: xmpp.$pres({
    xmlns: 'jabber:client',
    from: config.jid,
    to: `${config.room}/${config.jid.split('@')[0]}`,
  }).c('priority').t(100).up()
    .c('x', { xmlns: 'http://jabber.org/protocol/muc' }).up()
    .c('x', { xmlns: 'klavogonki:userdata' })
      .c('user')
        .c('login').t(config.login).up()
        .c('avatar').t(config.avatar).up()
        .c('background').t(config.color)
}
