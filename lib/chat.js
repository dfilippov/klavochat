const blessed = require('blessed');
const Client = require('./client');
const Tabs = require('./tabs');
const MessagesList = require('./messages-list');
const ParticipantsList = require('./participants-list');
const config = require('../config.json');
const gui = require('../gui.json');

module.exports = class Chat {
  constructor (screen) {
    this.screen = screen;
    this.loading = blessed.loading(gui.popup);
    this.message = blessed.message(gui.popup);
    this.message.hide();
    this.question = blessed.question(gui.popup);
    this.error = blessed.message(gui.error);
    this.error.hide();
    this.input = blessed.textbox(gui.input);
    this.input.on('submit', (message) => {
      this.sendMessage(message, this.messagesList.activeRoom);
    });
    this.tabs = new Tabs;
    this.messagesList = new MessagesList;
    this.participantsList = new ParticipantsList;
    this.participantsList.on('select', (user) => this.openDialog(user));
    screen.append(this.tabs.widget);
    screen.append(this.messagesList.widget);
    screen.append(this.participantsList.widget);
    screen.append(this.loading);
    screen.append(this.message);
    screen.append(this.question);
    screen.append(this.error);
    screen.append(this.input);
    this.input.focus();
    this.input.readInput();
    this.connect();
  }

  connect () {
    this.client = new Client;
    this.loading.load('Connecting to server...');
    this.client.on('online', () => this.loading.stop());
    this.client.on('error', (err) => this.crash(err));
    this.client.on('offline', (reason) => this.onOffline(reason));
    this.client.on('message', (message) => this.onMessage(message));
    this.client.on('presence', (presence) => this.onPresence(presence));
  }

  setRoom (id, title, state) {
    this.tabs.set(id, {
      text: title,
      state: 'unread',
      callback: () => this.messagesList.open(id),
    });
  }

  openDialog (user) {
    this.setRoom(user.id, user.login, 'item');
    this.messagesList.create(user.id);
    this.messagesList.open(user.id);
    this.tabs.active = user.id;
    this.input.clearValue();
    this.input.focus();
    this.input.readInput();
  }


  sendMessage (message, room) {
    this.client.sendMessage(message, this.messagesList.activeRoom);
    this.input.clearValue();
    this.input.readInput();
    this.screen.render();
  }

  onMessage (message) {
    const id = message.from.split('/')[0] === config.room ? config.room : message.from;
    const title = message.user ? message.user.login : id.split('@')[0];
    this.messagesList.add(message, id);
    this.setRoom(id, title, 'unread');
  }

  onPresence (presence) {
    if (presence.from === presence.to || presence.role === 'visitor') {
      return false;
    }

    if (presence.role !== 'none' && presence.user) {
      return this.participantsList.set(presence.jid, presence);
    }

    return this.participantsList.remove(presence.jid);
  }

  onOffline (reason) {
    const message = `Disconnected from server. ${reason.toString()}`;
    this.message.display(message, 1, () => {
      this.screen.destroy();
      console.log(message);
      process.nextTick(() => process.exit(0));
    });
  }

  close () {
    if (this.tabs.active === config.room) {
      this.question.ask('Exit?', (_, result) => {
        result ? this.client.disconnect() : this.screen.render();
      });
    } else {
      this.tabs.close(this.tabs.active);
    }
  }

  crash (err) {
    this.error.error(err.toString(), () => {
      this.screen.destroy();
      console.error(err);
      process.nextTick(() => process.exit(1));
    });
  }
};
